Proyecto de wordle
En este proyecto, hemos creado un wordle a través del lenguaje de programación "Kotlin". En este documento se especificarán, como se
juega, como funciona, que se puede mejorar y que hemos aprendido.

En primer lugar, Wordle es un juego que consiste en acertar una palabra, el usuario la escribe y el programa muestra las letras pintadas
en colores que muestran si está (entonces aparecerá en verde), si está mal colocada (en este caso saldrá amarillo) o si esa letra no está
(aparecerá en gris). Para mi proyecto, he introducido palabras que contienen 5 letras y el usuario dispone de 6 intentos para adivinarla.

A la hora de programar el juego, lo primero són establecer las variables, los colores, las palabras que pueden salir para adivinar, establecer 
el número de intentos, las líneas de juego y estableces que se van a introducir palabras. En primer lugar, se le pide la palabra al usuario (en
el caso de que introduzca una palabra que no contenga cinco letras, avisa al usuario con un mensaje en àntalla), a partir de aquí, el programa 
analiza las letras y muestra en pantalla las letras que están, las que están mal colocadas y las que no están, con sus respectivos colores; verde,
amarillo y gris. Una vez mostrado en pantalla, también aparecerán el número de intentos que lleva el jugador. Para que esto funcione, se encuentra en un bucle que solo acabará si la palabra introducida coincide con la palabra generada aleatoriamento o si se terminan los intentos; depende de 
que caso se de, se mostrara un mensaje u otro. Si acierta, aparecerá un mensaje de felicitación y si terminan los intentos, muestra que palabra era.Finalmente, se le pregunta al usuario si desea volver a jugar. Si presiona la tecla "s", todo el anterior proceso se repetirá, con una nueva palabra y con los intentos resetados; en caso contario, el juego termina y sale en pantalla el mensaje "gracias por jugar", finalizando el algoritmo.

En este proyecto siento que he aprendido un poco más acerca de este lenguaje de programación y haber sido capaz de sacar la copia de un juego
popular. También la incorporación de colores y como hacer que analice una secuencia (en este caso de letras) y te muestre si se encuentra un elemento.

Por otro lado, hay algunos aspectos que se podrian mejorar. Por ejemplo, si se escribe una letra repetida y la palabra tiene la misma letra, esta 
saldrá con el color repetido (por ejemplo, si la palabra es "tiempo" y el usuario escribe "tarta", ambas "t" saldrán de color verde, cuando la
primera tendría que ser verde y la segunda gris). Otra falla que le encuentro és que si el usuario escribe una palabra que no existe, no saldrá
ningún mensaje advirtiendo de que esa palabra no existe o no se encuentra en el diccionario, simplemente la da por buena, marca las letras que hay o no y te cuenta el intento. Esta parte no he podido incorporarla sin que me de problemas con el resto de líneas y he decidido dejarlo así, al fin y al cabo ayudarán también al jugador de una forma u otra. Otra diferencia con el juego original és que no he podido incorporar un teclado marcando que palabras se han utilizado, para ayudar al jugador a adivinar la palabra.

Para concluir, estoy satisfecha con el resultado final ya que es un juego de wordle básico y que recuerda bastente al original en cuanto a jugabilidad, el programa cumple con los requisitos y he podido incluir la función de volver a jugar, ya que en el anterior proyecto no fui capaz y queria añadir algo más para expandir el juego y que se sintiera más como tal y no como un simple proyecto de Kotlin. A pesar de lo que falta, lo que hay funciona y me quedo con lo que he aprendido.
